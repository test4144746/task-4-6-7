FROM alpine AS build
RUN apk update \
    && apk add maven \
    && apk add openjdk17
WORKDIR /app
COPY . /app

RUN mvn clean package

FROM alpine

COPY --from=build /app/target/*.jar /app.jar

EXPOSE 8080

CMD ["java", "-jar", "/app.jar"]
